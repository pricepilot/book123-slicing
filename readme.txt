[PROJECT INTRO]

1. Current production app - not a marketplace - written in AngularJS
https://narocanje.lassana.si

2. Marketplace demo app - written in Angular + Bootstrap
https://glam10.com

3. Now we are building the marketplace production app - written in Angular8 + IONIC 4
http://localhost:8100


[CUTTING ENV INSTALLATION]
git clone git@bitbucket.org:pricepilot/book123-slicing.git
cd book123-slicing

npm install -g sass
sass --no-source-map --watch  .:.

[CUTTING ENV STRUCTURE]:

├── assets
│   └── logo.svg
├── global.css // processed
├── global.scss //global scss file for global css classes and styling
├── ionic.css // core ionic css styling - do not change - override in global.scss
├── pages
│   ├── landing
│   │   ├── landing.css
│   │   ├── landing.html
│   │   └── landing.scss //only landing page related css classes and styling
│   └── login
│       ├── login.css
│       ├── login.html
│       └── login.scss //only login page related css classes and styling
└── theme.css //global theme settings like colors, padding...  -> https://ionicframework.com/docs/theming/advanced , https://ionicframework.com/docs/theming/colors

[IONIC BASICS]:

https://ionicframework.com/docs/components
https://ionicframework.com/docs/api/grid
https://ionicframework.com/docs/layout/css-utilities
https://ionicframework.com/docs/layout/structure

[UX WIREFRAMES AND STYLING GUIDE]

https://projects.invisionapp.com/share/3ATN6TGKZUS#/screens/380981368
https://projects.invisionapp.com/share/CER7JXLVGXZ

[RECOMMENDED CUTTING ORDER]

Do things that are clear and simple first and leave the parts where you are not sure about the positioning and styling for later - next week.

1) login page
2) Header and Footer part of the landing page
3) shop card on the listing page
4) shop-detail page
5) pick-slot page
6) review page
7) done page
...

In the next days we will add additional pages and elements to the repo.

[FINE TUNING]
Next week we will copy your css to our ionic project and we will do the fine tuning there. If you will take part
we will introduce you to the ionic cli development env, so no worries, step by step. 
